from pyspark.sql import SparkSession
from pyspark.sql import functions as F
import csv


def write_csv():
    with open('1_input.csv', 'w') as csvfile:
        filewriter = csv.writer(csvfile, delimiter=',', quotechar='|', quoting=csv.QUOTE_MINIMAL)
        filewriter.writerow(["id", "Name", "Surname", "Date"])
        filewriter.writerow([0, "John", "Smith", "1996-04-1"])
        filewriter.writerow([1, "Ashli", "Cruz", "1996-11-4"])
        filewriter.writerow([2, "Alex", "Johnson", "1984-08-23"])
        filewriter.writerow([3, "John", "Williams", "2004-07-15"])
        filewriter.writerow([4, "Mary", "Miller", "1958-11-17"])


def show_df_schema(df):
    df.show()
    df.printSchema()


if __name__ == "__main__":
    # start a new session
    spark = SparkSession \
        .builder \
        .appName("Python Spark SQL basic example") \
        .config("spark.some.config.option", "some-value") \
        .getOrCreate()
    write_csv()
    # Extract
    df = spark.read.load("1_input.csv", format="csv", sep=",", inferSchema="true", header="true")
    show_df_schema(df)
    #  Transforming
    df = df.withColumn("date", F.to_date(df.Date))
    df = df.withColumn("Age", (F.months_between(F.current_date(), df["Date"])/12).cast('int'))
    # Load Data
    df.write.csv('1_output.csv')
    show_df_schema(df)
    spark.stop()
